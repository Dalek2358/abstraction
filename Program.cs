﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
    //Создайте класс AbstractHandler.
    //В теле класса создать методы void Open(), void Create(), void Change(), void Save(). 
    //Создать производные классы XMLHandler, TXTHandler, DOCHandler от базового класса AbstractHandler.
    //Написать программу, которая будет выполнять определение документа и для каждого формата должны быть методы открытия, создания, редактирования, сохранения определенного формата документа.  

{
    abstract class AbstractHandler
    {
        public abstract void Open();
        public abstract void Create();
        public abstract void Change();
        public abstract void Save();
    }
    class XMLHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Документ в формате XML открыт");
        }
        public override void Create()
        {
            Console.WriteLine("Документ в формате XML Создан");

        }
        public override void Change()
        {
            Console.WriteLine("Документ в формате XML Изменен");

        }
        public override void Save()
        {
            Console.WriteLine("Документ в формате XML сохранен");

        }
    }
    class TXTHandler:AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Документ в формате TXT Открыт");

        }
        public override void Create()
        {
            Console.WriteLine("Документ в формате TXT создан");

        }
        public override void Change()
        {
            Console.WriteLine("Документ в формате TXT изменен");

        }
        public override void Save()
        {
            Console.WriteLine("Документ в формате TXT сохранен");

        }
    }
    class DOCHandler:AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Документ в формате DOC открыт");

        }
        public override void Create()
        {
            Console.WriteLine("Документ в формате DOC создан");

        }
        public override void Change()
        {
            Console.WriteLine("Документ в формате DOC изменен");

        }
        public override void Save()
        {
            Console.WriteLine("Документ в формате DOC сохранен");

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите формат документа(XML, TXT, DOC): ");
            string format = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Что требуется сделать с документом: 1 - открыть; 2 - создать; 3 - изменить; 4 - сохранить");
            string action = Console.ReadLine();

            AbstractHandler handler;
            
            switch (action)
            {
                case "1":
                    {
                        if (format == "XML") { handler = new XMLHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "TXT") { handler = new TXTHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "DOC") { handler = new DOCHandler() as AbstractHandler; handler.Open(); }
                        else { Console.WriteLine("Вы ввели недопустимый формат"); }
                        break;
                    }
                case "2":
                    {
                        if (format == "XML") { handler = new XMLHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "TXT") { handler = new TXTHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "DOC") { handler = new DOCHandler() as AbstractHandler; handler.Open(); }
                        else { Console.WriteLine("Вы ввели недопустимый формат"); }
                        break;
                    }
                case "3":
                    {
                        if (format == "XML") { handler = new XMLHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "TXT") { handler = new TXTHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "DOC") { handler = new DOCHandler() as AbstractHandler; handler.Open(); }
                        else { Console.WriteLine("Вы ввели недопустимый формат"); }
                        break;
                    }
                case "4":
                    {
                        if (format == "XML") { handler = new XMLHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "TXT") { handler = new TXTHandler() as AbstractHandler; handler.Open(); }
                        else if (format == "DOC") { handler = new DOCHandler() as AbstractHandler; handler.Open(); }
                        else { Console.WriteLine("Вы ввели недопустимый формат"); }
                        break;
                    }
            }

            //Delay
            Console.ReadKey();
        }
    }
}
